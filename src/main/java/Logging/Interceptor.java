/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logging;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.AroundConstruct;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author joshbeck
 */

public class Interceptor implements Serializable {
    @EJB
    ProjectLog logger;
    
    @AroundConstruct
    public void setUpCode(){
        logger.logOutput("Initializing the Interceptor");
    }
    @AroundInvoke
    public Object methodInterceptor(InvocationContext ctx) throws Exception{
        String className = ctx.getTarget().getClass().getName();
        String methodName = ctx.getMethod().getName();
        
        logger.logOutput("Method " + methodName + " was called from " + className);

        return ctx.proceed();
    }
    
    
}
