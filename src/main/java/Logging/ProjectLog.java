/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logging;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.ejb.Singleton;

/**
 *
 * @author joshbeck
 */

////Run code on startup
@Startup
@Singleton
public class ProjectLog {
    
    //Whenever the log is initialized, run the following code
    @PostConstruct
    public void init() {
        System.out.println("LOGGING: Setting up the project log");
    }
    
    
    //Custom log method
    public void logOutput(String msg){
        System.out.println("OUPUT FROM LOG: " + msg);
    }
}
