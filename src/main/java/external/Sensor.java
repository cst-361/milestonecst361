/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package external;

import Logging.Interceptor;
import database.SensorListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.inject.Singleton;
import javax.interceptor.Interceptors;
import models.PersonDataPacket;

/** This class handles simulating a door sensor for the Milestone project.
 *  It will be stimulated by a driver class with various time periods.
 *  Sensor output is randomly generated and notifies a SensorListener
 * @author Josh
 */
@Interceptors(Interceptor.class)
@Stateless
public class Sensor {
    private Random randomGen = new Random();
    private List<SensorListener> listeners = new ArrayList<SensorListener>();
    //This method will occasionally read a person but should be called rythmically
    //from the sensor driver class to simulate the populatation of a database.
    //Time should be provided to represent the simulated time
    public void readSensor(Double time){
        //Randomly call personEnter() or personExit() methods
        
        //1 out of every 10 times have a person enter or exit
        if (randomGen.nextInt(10) == 0){
            if (randomGen.nextBoolean()){
                doorStateChange(new PersonDataPacket(time, true));
            } else {
                doorStateChange(new PersonDataPacket(time, false));
            }
                    
        }
    }
    
    //Handle a person entering or exiting connoted by the person packet data
    public void doorStateChange(PersonDataPacket person){
        //Call sensor listener person enter
        for (SensorListener s: listeners){
            s.save(person);
        }
    }

    public List<SensorListener> getListeners() {
        return listeners;
    }

    public void addListener(SensorListener listener) {
        this.listeners.add(listener);
    }
}
