/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package external;

import Logging.Interceptor;
import database.SensorListener;
import javax.inject.Singleton;
import javax.interceptor.Interceptors;

/** Handles setting up the emulated door sensor, populating it with data, 
 * and adding a listener to update the database
 *
 * @author Josh
 */
@Interceptors(Interceptor.class)
@Singleton
public class SensorDriver {
    Sensor doorSensor;
    public SensorDriver(){
        //Holds singular instance ot the Sensor
        Sensor doorSensor = new Sensor();
        doorSensor.addListener(new SensorListener());
    }
    //Simulates twenty four hours of the sensor
    public void simulateTwentyFourHours(){
        //Simulate time ticking in increments of 1/100th of an hour
        for (Double i = 0.0; i < 24.0; i+=0.01){
            doorSensor.readSensor(i);
        }
    }
}
