/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import Logging.Interceptor;
import java.util.*;
import models.*;
import database.*;
import external.SensorDriver;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.swing.JOptionPane;

import javax.faces.view.*;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.interceptor.Interceptors;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;


/** This controller handles the data fetching and calculations in order
 * to be displayed by an XHTML file
 *
 * @author Josh
 */
@Named("displayController") @ViewScoped @Interceptors(Interceptor.class)
public class DisplayController implements Serializable {

    int startTime;
    int endTime;

    List<PersonDataPacket> getPeople() {
        return people;
    }

    void setPeople(List<PersonDataPacket> people) {
        this.people = people;
    }

    List<PersonDataPacket> people;
    int getStartTime() {
        return startTime;
    }

    void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    int getEndTime() {
        return endTime;
    }

    void setEndTime(int endTime) {
        this.endTime = endTime;
    }
    
    void populateDatabaseWithData(){
        //Create only instance of sensor driver in charge of managing and populating the sensor
        SensorDriver sensorDriver = new SensorDriver();
        sensorDriver.simulateTwentyFourHours();
    }

    public String fetchData(){

        PersonHandler dataAccess = new PersonHandler();
        people = dataAccess.getPeople();
        
         //Save data to Context
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("people", people);
       
        return "/DisplayData.xhtml";
    }
    public BarChartModel createModel(List<PersonDataPacket> people) {
        BarChartModel barModel = populateModel(people);
 
        barModel.setTitle("Sensor Readings by Hour for past 24 Hours");
        barModel.setLegendPosition("ne");
 
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Hour of the Day");
 
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Total People");
        return barModel;
    }
    BarChartModel populateModel(List<PersonDataPacket> people){
        BarChartModel model = new BarChartModel();
 
        ChartSeries enterData = new ChartSeries();
         ChartSeries exitData = new ChartSeries();
        //Create array holding how many enters in 24 hours
        int[] enters = new int[24];
        int[] exits = new int[24];
       
        for (PersonDataPacket p: people){
           
            if (p.entering == true){
                //Add to entering set
                //Add to closest hour
                enters[(int)Math.floor(p.time)] += 1;
            } else {
                //Add to the leaving set
                exits[(int)Math.floor(p.time)] += 1;
            }
            
        }

        enterData.setLabel("People Who Entered");
        exitData.setLabel("People Who Exited");
        
        //Add data to the model
        for (int i=0;i < 24; i++){
            enterData.set(i, enters[i]);  
        }
         
        for (int i=0;i < 24; i++){
             exitData.set(i, exits[i]);
        }
           
        model.addSeries(enterData);
        model.addSeries(exitData);
 
        return model;
    }
    
}
