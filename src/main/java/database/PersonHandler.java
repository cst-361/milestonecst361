/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
import models.*;
import java.util.*; 
/** This class handles fetching information from the database regarding the 
 * people entering and exiting
 *
 * @author Josh
 */
public class PersonHandler {
    public List<PersonDataPacket> getPeople(){
        /** ==============================================
        *  Implement method to access database, do a SQL query for entries 
        *  for past 24 hours, and parse this data into a List of PersonPacket
        *  type objects to be handled by controllers.
        */
        
        //PLACEHOLDER DUMMY VALUES
        List<PersonDataPacket> l = new ArrayList<>();
        l.add(new PersonDataPacket(1.2, true));
        l.add(new PersonDataPacket(1.1, true));
        l.add(new PersonDataPacket(3.2, false));
        l.add(new PersonDataPacket(9.2, true));
        l.add(new PersonDataPacket(0.5, false));
        l.add(new PersonDataPacket(20.1, true));
        l.add(new PersonDataPacket(1.3, true));
        
        return l;
    }
    
    
    //Default contructor
    public PersonHandler(){
        //Do any additional set up other than starting a connection
        
        startConnection();
    }
    
    //To-do
    private void startConnection(){
        /** =======================================
         * Enter code to start a connection to the database
         */
    }
    
    //To-do
    //Method to handle adding another person (either entry or exit as denoted in packet)
    public void addPerson(PersonDataPacket person){
        /** ========================================
         * Implement code to add person to the database with a given PersonPacket
         */
    }
}
