/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import Logging.Interceptor;
import Logging.ProjectLog;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.interceptor.Interceptors;
import web.service.UsersFacadeREST;
/**
 *
 * @author Josh
 */
@Named("user") @javax.faces.view.ViewScoped @Interceptors(Interceptor.class)
public class User implements Serializable {
    //Create a new connection to the database
     void createNewConnection(){
        
    }
    //The page ID of the page to be segued to if login/registration was successful
     String successPage = "/GetDataRange.xhtml";
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getValidatePassword() {
        return validatePassword;
    }

    public void setValidatePassword(String validatePassword) {
        this.validatePassword = validatePassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    User(){
        //Create new connection to database
        getConnection();
    }
    //Get the connection to the database
    void getConnection(){
        
    }
    //HOLD THE SIGN IN MANAGED BEAN
    String username;
    String password;
    

    public String signIn(){
        
        try {
            System.out.println("SIGNIGN IN");
            if (new UsersFacadeREST().login(username, password) != null){
           return successPage + "?" + (new UsersFacadeREST().login(username, password)).getEmail();
            } else {
                System.out.println("Login was unsuccessfull bc no entry was found in the database");
                return "/Failure.xhtml";
            }
        } catch (Exception e){
            return "/Failure.xhtml";
        }
        
    }
    
    //HOLD THE REGISTER MANAGED BEAN
    String name;
    String newPassword;
    String validatePassword;
    String email;
    

    public String registerAccount(String name, String newPassword, String validatePassword, String email){
        //Return display screen if successfully created
        //Otherwise, display failure screen
        
        try {
            System.out.println("SIGNIGN IN");
            if (new UsersFacadeREST().register(name, newPassword, email)){
                return successPage;
            } else {
                return "/Failure.xhtml";
            }
            
        } catch (Exception e){
            return "/Failure.xhtml";
        }
    }
}
