/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

/** This class holds the data regarding entering or exiting by a person captured
 * by the database
 *
 * @author Josh
 */
@ManagedBean(name="personDataPacket", eager=true)
@ViewScoped()
public class PersonDataPacket {
    //Held in value from 0...24 representing hours & percent of hours (not minutes or seconds)
    public Double time;
    //Holds value of whether the person was entering; otherwise, exiting
    public boolean entering;
    
    public PersonDataPacket(Double time, boolean entering){
        this.time = time;
        this.entering = entering;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public boolean isEntering() {
        return entering;
    }

    public void setEntering(boolean entering) {
        this.entering = entering;
    }
}
