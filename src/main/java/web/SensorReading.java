/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Josh
 */
@Entity
@Table(name = "sensor_reading")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SensorReading.findAll", query = "SELECT s FROM SensorReading s"),
    @NamedQuery(name = "SensorReading.findById", query = "SELECT s FROM SensorReading s WHERE s.id = :id"),
    @NamedQuery(name = "SensorReading.findByEventTime", query = "SELECT s FROM SensorReading s WHERE s.eventTime = :eventTime"),
    @NamedQuery(name = "SensorReading.findByEntered", query = "SELECT s FROM SensorReading s WHERE s.entered = :entered")})
public class SensorReading implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entered")
    private int entered;

    public SensorReading() {
    }

    public SensorReading(Integer id) {
        this.id = id;
    }

    public SensorReading(Integer id, Date eventTime, int entered) {
        this.id = id;
        this.eventTime = eventTime;
        this.entered = entered;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public int getEntered() {
        return entered;
    }

    public void setEntered(int entered) {
        this.entered = entered;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SensorReading)) {
            return false;
        }
        SensorReading other = (SensorReading) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "web.SensorReading[ id=" + id + " ]";
    }
    
}
