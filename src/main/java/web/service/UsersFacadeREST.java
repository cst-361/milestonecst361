/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import Logging.Interceptor;
import java.sql.SQLException;
import web.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Josh
 */
@Stateless
@Path("users")
@Interceptors(Interceptor.class)
public class UsersFacadeREST extends AbstractFacade<Users> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManagerFactory entityManagerFactory;

    public UsersFacadeREST() {
        super(Users.class);
        
    }

   
    

  

    @Override
    protected EntityManager getEntityManager() {
      
        return  entityManagerFactory.createEntityManager();
    }
    
    @GET
    @Path("login")
    @Produces("application/json")
    @Override
    public Users login(@QueryParam("username") String u, @QueryParam("password") String p) throws ClassNotFoundException, SQLException, SQLException, InstantiationException, IllegalAccessException{
        //Links the query to the login method contained in the abstract facade super class
      
        return super.login(u, p);
       
    }
    
    @GET
    @Path("register")
    @Override
    public boolean register(@QueryParam("username") String u, @QueryParam("password") String p, @QueryParam("email") String e) {
        //Links the query to the login method contained in the abstract facade super class
        
        return super.register(u, p, e);
        
       
    }
    @Path("/test")
    @GET
    @Produces("text/plain")
    public String methode_test()
    {
        return "Hello test";
    }
}
