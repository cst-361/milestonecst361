/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import Logging.Interceptor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import web.Users;

/**
 *
 * @author Josh
 */
@Interceptors(Interceptor.class)
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    
    //Get the login result for the user
    public T login(String u, String p) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
        
        Connection conn = null;
                
               
		String url = "jdbc:mysql://localhost:3306/milestone361?useSSL=false";
		String username = "root";
		String password = "password";
		
		
		try 
		{
			// Connect to the Database
                        System.out.println("Able to connect to the database");
			Class.forName("com.mysql.jdbc.Driver").newInstance();  
                        
                         conn=DriverManager.getConnection(url,username,password); 
                         System.out.println("Able to connect");
                         String sql = "SELECT * FROM users WHERE password = '" + p + "' AND username = '" + u + "'";
                         
                        Statement stmt1 = conn.createStatement();
                        System.out.println("Created statement");
			ResultSet rs1 = stmt1.executeQuery(sql);
                        System.out.println("Executed query");
                       
                    try {
                        System.out.println("Found user for query" + rs1.next() + rs1.getString("USERNAME") + rs1.getString("EMAIL"));
                            return (T) new Users(0, rs1.getString("USERNAME"), rs1.getString("PASSWORD"), rs1.getString("EMAIL"));
			
                    } catch (Exception e) {
                        System.out.println("Querry returned null so don't login");
                                return null;
                    }
			
		
                } catch (Exception e) {
                    System.out.println("There was an error " + e.getMessage());
                }
                return null;
        
    }

   public boolean register(String u, String p, String e) {
       System.out.println("HELLO WORLD");
        
    System.out.println("HELLO WORLD 2");
       
                
               
		String url = "jdbc:mysql://localhost:3306/milestone361?useSSL=false";
		String username = "root";
		String password = "password";
                
			try {
			
                        System.out.println("HELLO WORLD 5");
                         Connection conn=DriverManager.getConnection(url,username,password); 
                   System.out.println("HELLO WORLD 4");
                         String sql = "INSERT INTO users VALUES (NULL, '" + u + "', '" + p + "', '" + e + "')";
                         
                        Statement stmt1 = conn.createStatement();
                       System.out.println("HELLO WORLD 3");
			if (stmt1.executeUpdate(sql) != 1){
                            return false;
                        }
                       
                       conn.close();
                       return true;
                        } catch (SQLException error){
                            System.out.println(error.getLocalizedMessage());
                        }
                    
			return false;
		
               
              
   }
    
}
